/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.anadi.siege.domain;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 *
 * @author ruben
 */
public class SiegeParameters {
    
    /**
     * Interval in seconds
     */
    private final int interval;
    
    /**
     * Service port to contact
     */
    private final int port;
    
    /**
     * The host of the service Internet address
     */
    private final InetAddress node;
    
    /**
     * Timeout in milliseconds
     */
    private final int timeout;
    
    /**
     * Duration in seconds
     */
    private final int duration;
    
    public SiegeParameters(int interval, int port, String addr,
            int timeout, int duration) throws UnknownHostException {
        this.interval = interval;
        this.port = port;
        this.node = InetAddress.getByName(addr);
        this.timeout = timeout;
        this.duration = duration;
    }

    public int interval() {
        return interval;
    }

    public int port() {
        return port;
    }

    public InetAddress node() {
        return node;
    }

    public int timeout() {
        return timeout;
    }

    public int duration() {
        return duration;
    }
    
    
}

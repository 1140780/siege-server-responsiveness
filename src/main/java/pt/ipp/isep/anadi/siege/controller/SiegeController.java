/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.anadi.siege.controller;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledFuture;
import pt.ipp.isep.anadi.siege.domain.Siege;
import pt.ipp.isep.anadi.siege.domain.SiegeLog;
import pt.ipp.isep.anadi.siege.domain.SiegeParameters;

/**
 *
 * @author ruben
 */
public class SiegeController {

    private SiegeParameters params;
    
    private Siege siege;


    public boolean setParams(String address, int port, int interval, int timeout, int duration) {
        SiegeParameters tempParams;
        try {
            tempParams = new SiegeParameters(interval, port, address, timeout, duration);
        } catch (UnknownHostException ex) {
            return false;
        }
        this.params = tempParams;
        return true;
    }

    public void writeToPath(String path) {

    }

    public void startSiege(SiegeLog log) throws IOException {
        if (null == this.params) {
            throw new IllegalArgumentException("Missing Parameters");
        }
        this.siege = new Siege();
        siege.startSiege(params, log);
    }

    public void stopSiege() {
        if (null != this.siege)
            siege.stopSiege();
    }

    public void saveLog(File selectedFile) throws IOException {
        siege.saveSiegeLog(selectedFile);
        
    }
}

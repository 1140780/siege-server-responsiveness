/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.anadi.siege.domain;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.TemporalAccessor;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.anadi.siege.UI.SiegeWindow;

/**
 *
 * @author ruben
 */
public class Siege {

    private ScheduledExecutorService executor;

    private SiegeParameters params;

    private SiegeLog log;

    private ServerConnection server;

    private ServiceState state;

    public void startSiege(SiegeParameters params, SiegeLog log) throws IOException {
        this.params = params;
        this.log = log;
        this.state = new ServiceState();
        log.addHeaders(header());
        executor = Executors.newSingleThreadScheduledExecutor();
        server = new ServerConnection(params, log, state);
        Long interval = new Long(params.interval());
        executor.scheduleAtFixedRate(server, 0, interval, TimeUnit.SECONDS);
        awaitExecution();
    }

    public void stopSiege() {
        int testDuration = (int) ((System.currentTimeMillis() - this.state.startTime()) / 1000L);
        this.state.duration(testDuration);
        executor.shutdownNow();
        try {
            executor.awaitTermination(this.params.interval(), TimeUnit.SECONDS);
        } catch (InterruptedException ex) {
            //
        }
        log.append(footer());
        log.notifyObservers();
    }

    private String header() {
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);
        TemporalAccessor date = LocalDateTime.now();
        String header = formatter.format(date) + " - New Siege test started:\n";
        header = header.concat("INTERVAL: " + params.interval() + "\n");
        header = header.concat("NODE: " + params.node() + " - PORT: " + params.port() + "\n");
        header = header.concat("TIMEOUT: " + params.timeout() + "\n");
        header = header.concat("DURATION: " + params.duration() + "\n");
        return header;
    }

    private String footer() {
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);
        TemporalAccessor date = LocalDateTime.now();
        String footer = "\n" + formatter.format(date) + " - Siege test ended:\n";
        footer += "TOTAL NR. CONNECTIONS: " + this.server.numberOfConnectionsMade() + "\n";
        String availability = String.format("%.2f", this.state.availability()*100);
        footer += "AVAILABILITY: " + availability
                + "% ( " + this.server.numberOfSuccessfullConnections() + " / "
                + this.server.numberOfConnectionsMade() + " )\n";
        String mtbf = this.state.humanReadableMTBF();
        footer += "MTBF: " +mtbf + " (" + this.state.meanTimeBetweenFailures() +" seconds)"+ "\n";
        return footer;
    }

    private void awaitExecution() {
        ExecutorService exec = Executors.newSingleThreadExecutor();
        exec.execute(() -> {
            try {
                boolean result = executor.awaitTermination(Long.valueOf(params.duration()), TimeUnit.SECONDS);
                if (!result) {
                    stopSiege();
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(SiegeWindow.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    public void saveSiegeLog(File selectedFile) throws IOException {
        String text = this.log.toString();
        List<String> lines = Arrays.asList(text.split("\n"));
        Files.write(selectedFile.toPath(), lines, Charset.forName("UTF-8"));
    }

}

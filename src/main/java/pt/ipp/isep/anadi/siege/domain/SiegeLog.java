/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.anadi.siege.domain;

import java.util.Observable;

/**
 *
 * @author ruben
 */
public class SiegeLog extends Observable {
    
    private String log;

    public SiegeLog() {
        this.log = new String();
    }
    
    public String append(String text) {
        this.setChanged();
        this.log = this.log.concat(text+"\n");
        return this.log;
    }
    
    public String clear() {
        this.log = "";
        return this.log;
    }

    @Override
    public String toString() {
        return this.log;
    }

    public String addHeaders(String text) {
        this.log = text.concat("\n"+this.log);
        return this.log;
    }    
    
}

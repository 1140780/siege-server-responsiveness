/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.anadi.siege.domain;

import java.util.concurrent.TimeUnit;

public class ServiceState {

    private int duration;
    private long downtime;
    private boolean wasDown;
    private int downUpCicles;
    private long lastDownTime;
    private long startTime;
    
    private int successes;
    private int total;

    public void duration(int duration) {
        this.duration = duration;
    }

    public long startTime() {
        return this.startTime;
    }

    public void setDown() {
        this.total++;
        if (this.startTime == 0) {
            this.startTime = System.currentTimeMillis();
        }

        if (lastDownTime > 0) {
            downtime += System.currentTimeMillis() - lastDownTime;
        }

        lastDownTime = System.currentTimeMillis();
        wasDown = true;
    }

    public void setUp() {
        this.successes++;
        this.total++;
        if (this.startTime == 0) {
            this.startTime = System.currentTimeMillis();
        }

        if (wasDown) {
            downUpCicles++;
            downtime += System.currentTimeMillis() - lastDownTime;
        }
        
        wasDown = false;
    }

    public double availability() {
        /*
        double uptime = this.duration - (downtime / 1000L);
        System.out.println("UPtime: " + uptime + " downtime: " + downtime / 1000L + " duration:" + duration);
        if (uptime < 1) {
            return 0.0;
        }
        return uptime / duration;
        */
        return (double)successes/total;
    }

    public double meanTimeBetweenFailures() {
        double uptime = this.duration - (downtime / 1000L);
        System.out.println("sum: " + uptime);
        System.out.println("downup cicles: " + downUpCicles);
        return uptime / downUpCicles;
    }

    public String humanReadableMTBF() {
        if (downUpCicles == 0) {
            return "N/A";
        }
        long mtbf = (long) Math.round(meanTimeBetweenFailures());

        long days = TimeUnit.SECONDS
                .toDays(mtbf);
        mtbf -= TimeUnit.DAYS.toSeconds(days);

        long hours = TimeUnit.SECONDS
                .toHours(mtbf);
        mtbf -= TimeUnit.HOURS.toSeconds(hours);

        long minutes = TimeUnit.SECONDS
                .toMinutes(mtbf);
        mtbf -= TimeUnit.MINUTES.toSeconds(minutes);

        long seconds = mtbf;
        String mtbfStr = "";
        if (days > 0) {
            if (days == 1) {
                mtbfStr += days + " day";
            } else {
                mtbfStr += days + " days";
            }
        }
        if (hours > 0) {
            if (hours == 1) {
                mtbfStr += hours + " hour";
            } else {
                mtbfStr += hours + " hours";
            }
        }
        if (minutes > 0) {
            if (minutes == 1) {
                mtbfStr += minutes + " minute";
            } else {
                mtbfStr += minutes + " minutes";
            }
        }
        if (seconds > 0) {
            if (seconds == 1) {
                mtbfStr += seconds + " second";
            } else {
                mtbfStr += seconds + " seconds";
            }
        }

        return mtbfStr;
    }
}

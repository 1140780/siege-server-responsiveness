/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.anadi.siege.domain;


import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

/**
 *
 * @author ruben
 */
public class ServerConnection implements Runnable {

    private final SocketAddress target;
    private final SiegeParameters params;
    private final SiegeLog log;
    private final ServiceState state;
    private int attempts;
    private int successes;
    
    public ServerConnection(SiegeParameters params, SiegeLog log, ServiceState state) throws IOException {
        this.params = params;
        this.target = new InetSocketAddress(params.node(), params.port());
        this.log = log;
        this.attempts = 0;
        this.state = state;
    }

    @Override
    public void run() {
        this.attempts++;
        try {
            try (Socket client = new Socket()) {
                long startTime = System.currentTimeMillis();
                client.connect(this.target, this.params.timeout());
                long elapsedTime = System.currentTimeMillis() - startTime;
                String logMsg = "#"+attempts+";"+System.currentTimeMillis()/1000L+";UP;" + elapsedTime +" ms";
                System.out.println(logMsg);
                this.log.append(logMsg);
                this.state.setUp();
                this.log.notifyObservers(log);
                this.successes++;
            }
        } catch (Exception ex) {
            String logMsg = "#"+attempts+";"+System.currentTimeMillis()/1000L+";DOWN;"+ex.getMessage();
            System.out.println(logMsg);
            this.log.append(logMsg);
            this.state.setDown();
            this.log.notifyObservers(log);
        }            
        
    }
    
    public double availability() {
        return this.state.availability();
    }
    
    public double meanTimeBetweenFailures() {
        return this.state.meanTimeBetweenFailures();
    }
    
    public int numberOfConnectionsMade() {
        return this.attempts;
    }

    public int numberOfSuccessfullConnections() {
        return this.successes;
    }

}

# README #

This Java application pings a given host for a given time, then outputs/saves statistics about its availability

![SiEGE](https://bytebucket.org/1140780/siege-server-responsiveness/raw/master/SIEGE.png)

Just compile and run from IDE or the generated FAT jar in target folder.